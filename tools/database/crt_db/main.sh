#!/usr/bin/env bash
# vim: set noexpandtab tabstop=2:

set -v

user=$1
psd=$2

mysql -u "$user" -p"$psd" <<EOF
create database curation_proj;
EOF

